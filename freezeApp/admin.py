from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
import subprocess
import os, signal

# Register your models here.

from .models import Multicast
from .models import Alarma
from .models import Programa


@admin.register(Multicast)
class MulticastAdmin(admin.ModelAdmin):
    list_display = ("IP", "estado",)

    actions = ["monitorear","matar"]

    def monitorear(self, request, queryset):
        for multicast in queryset:
            if (multicast.estado == 'NM'):         
                r = subprocess.Popen(["python3", "manage.py", "monitorear", str(multicast), str(multicast.puerto_retransmision)])
                multicast.PID = r.pid
                multicast.estado = 'MO'
                multicast.save()
 
    def matar(self, request, queryset):
        for multicast in queryset:
            try:
                #os.kill(multicast.PID, signal.SIGKILL)
                multicast.estado = 'NM'
                multicast.save()
            except OSError:
                break
                	
@admin.register(Alarma)
class AlarmaAdmin(admin.ModelAdmin):
    list_display = ("fecha", "Programa", "reestablecimiento")

@admin.register(Programa)
class ProgramaAdmin(admin.ModelAdmin):
    list_display = ("programa",)


