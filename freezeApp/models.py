from django.db import models
from django.utils import timezone
import json
import  requests
from collections import Counter
import subprocess
from time import sleep
import os
from . import mapeo

# Create your models here.

class Programa(models.Model):
    programa = models.CharField(max_length = 200)
    def __str__(self):
        return(self.programa)


class Multicast(models.Model):
    IP = models.CharField(max_length = 200)
    Programas = models.ManyToManyField('Programa')
    PID = models.IntegerField(blank = True, null = True)
    puerto_retransmision = models.CharField(max_length = 200, unique = True, blank = True)
    no_monitoreando = 'NM'
    monitoreando = 'MO'
    MONITOR_CHOICES = [
        (monitoreando, 'monitoreando'),
        (no_monitoreando, 'no monitoreando'),
    ]
    estado = models.CharField(
        max_length=2,
        choices=MONITOR_CHOICES,
        default = 'NM',
    )

    def __str__(self):
        return(self.IP)

class Alarma(models.Model):
    Programa = models.CharField(max_length = 200, null = True)
    multicast = models.CharField(max_length = 200, null = True)
    fecha = models.DateTimeField(auto_now_add = True, blank = True, null = True)
    reestablecimiento = models.DateTimeField(auto_now = True, blank = True, null = True)

    def __str__(self):
        return(self.Programa)


