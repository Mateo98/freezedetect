from django.apps import AppConfig


class FreezeappConfig(AppConfig):
    name = 'freezeApp'
