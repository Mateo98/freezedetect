import json
import subprocess
from time import sleep
import os
import  requests
from collections import Counter
from .mapeo import crear_mapeo_streams
from .mapeo import crear_mapeo_programas
from .mapeo import crear_programa_mapeo
from freezeApp.models import Alarma
from freezeApp.models import Multicast

class Tabla_programa_mapeo:
    def __init__(self, programa, stream, estado, alarma_id):
        self.programa = programa
        self.stream = stream
        self.estado = estado
        self.alarma_id = alarma_id

def monitorear_multicast(mult_entrada, puerto_retrans):
    cmnd_copy = ["ffmpeg", "-i", "udp://"+mult_entrada, "-map", "0:v", "-c", "copy", "-f", "mpegts", "udp://229.0.0.3:"+puerto_retrans]
    proc_copy = subprocess.Popen(cmnd_copy, stdout=subprocess.PIPE, stderr=open("mapeo"+mult_entrada+".txt", "w"))

    sleep(6)
    #i=0
    f =  open("mapeo"+mult_entrada+".txt", "r")

    programas_mapeo = crear_programa_mapeo(f)

    for entrada in programas_mapeo:
        print(entrada.programa, entrada.stream, entrada.estado, sep = ' ')

    archivo_tabla = open("tabla"+mult_entrada+".txt", "w")
    
    for entrada in programas_mapeo:
        archivo_tabla.write(entrada.programa + entrada.stream + entrada.estado + "\n")


    #proc_copy.terminate()

    #os.remove("mapeo.txt")
    print("empece")
    sleep(10)


    while(1):
        #print (i)
        for j in programas_mapeo:
            parseado=j.stream[2:]
            archivito = "freeze"+parseado+".txt"
            cmnd_freeze = ["ffmpeg", "-i", "udp://229.0.0.3:"+puerto_retrans, "-vf", "freezedetect=n=-60dB:d=10,metadata=mode=print:file="+archivito, "-map", j.stream, "-f", "null", "-"]
            print(cmnd_freeze)
            proc_freeze = subprocess.Popen(cmnd_freeze, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            sleep(20)
            if (os.path.isfile(archivito)==False):
                f= open(archivito,"r+")
            proc_freeze.terminate()
            out, err =  proc_freeze.communicate()
            print ("==========output==========")

            if (os.stat(archivito).st_size != 0 and j.estado == "corriendo"):
                print("voo el programa "+j.programa+" "+"ta congelandose")
                j.estado = "congelado"
                alarma_nueva = Alarma(Programa=j.programa, multicast=mult_entrada)
                alarma_nueva.save()
                j.alarma_id = alarma_nueva.id
                print(j.alarma_id)
                os.remove(archivito)

            elif (os.stat(archivito).st_size == 0 and j.estado == "congelado"):
                print("voo el programa"+j.programa+" "+"se destranco")
                alarma = Alarma.objects.get(id = j.alarma_id)
                alarma.save()
                j.estado = "corriendo"

        multicast_monitoreada = Multicast.objects.get(IP = mult_entrada)
        if (multicast_monitoreada.estado == 'NM'):
            print("no monitoreando")
            break
       
        #i=i+1
    print('hola')
    proc_copy.terminate()
    print("termine proceso")