from django.core.management.base import BaseCommand, CommandError
from freezeApp.models import Multicast

class Command(BaseCommand):
    help = 'Imprime multicasts'

    def add_arguments(self, parser):
        parser.add_argument('multicast_id', type = str)

    def handle(self, *args, **options):
        multicast_id = options['multicast_id']
        print(multicast_id)
