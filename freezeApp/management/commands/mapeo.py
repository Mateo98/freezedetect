import  requests
from collections import Counter
import subprocess
from time import sleep
import os

class Tabla_stream_mapeo:
    def __init__(self, stream, mapeo):
        self.stream = stream
        self.mapeo = mapeo

class Tabla_programa_stream:
    def __init__(self, programa, stream):
        self.programa = programa
        self.stream = stream

class Tabla_programa_mapeo:
    def __init__(self, programa, stream, estado, alarma_id):
        self.programa = programa
        self.stream = stream
        self.estado = estado
        self.alarma_id = alarma_id


def crear_mapeo_streams(info):


    # Primero creo los contenidos de la tabla, para eso necesito saber de donde a donde va la tabla

    inicio = info.find("Stream mapping:")+16
    print ("el inicio de la tabla es" )
    print (inicio)
    print ("\n")
    fin_tabla = info.find("Press [q] to stop, [?] for help")
    print ("el fin de la tabla es" )
    print (fin_tabla)
    print("\n")
    Tabla_content = info[inicio:fin_tabla]

    # Me fijo que hayan quedado bien los contenidos de la tabla

    limites_tabla = open("limites_streams.txt", "w")
    limites_tabla.write(Tabla_content)

    # Hago la tabla  

    tabla = []

    for line in Tabla_content.splitlines():
        stream = line [line.find("#")+1: line.find(" ->") ]
        stream_mapeado = line [line.rfind("#")+1: line.find(" (copy)") ]
        tabla.append(Tabla_stream_mapeo(stream, stream_mapeado))

    archivo_tabla = open("mapeo_streams.txt", "w")
    for entrada in tabla:
        archivo_tabla.write(entrada.stream + entrada.mapeo + "\n")


    return tabla

def crear_mapeo_programas(info):

    # Primero creo los contenidos de la tabla, para eso necesito saber de donde a donde va la tabla

    inicio = info.find("Input #0")
    fin_tabla = info.find("Output #0")
    Tabla_content = info[inicio:fin_tabla]

    # Me fijo que hayan quedado bien los contenidos de la tabla

    fila_completa = False
    tabla = []

    for line in Tabla_content.splitlines():
        if "service_name" in line:
            programa = line [line.find(": ")+2 :]

        if "Video:" in line:
            stream = line [line.find("#")+1: line.find("[") ]
            fila_completa = True

        if fila_completa:
             tabla.append(Tabla_programa_stream(programa, stream))
             fila_completa = False
    
    archivo_tabla = open("mapeo_programas.txt", "w")
    for entrada in tabla:
        archivo_tabla.write(entrada.programa + entrada.stream + "\n")

    return tabla

def crear_programa_mapeo(f):

   info = f.read()

   tabla_streams = []
   tabla_programas = []
   tabla_programas = crear_mapeo_programas(info)
   tabla_streams = crear_mapeo_streams(info)
   tabla_con_estado_mapeo = []

   for j in tabla_programas:
       for i in tabla_streams: 
           if (j.stream == i.stream):
               j.stream = i.mapeo

   for j in tabla_programas:
       programa = j.programa
       mapeo = j.stream
       estado = "corriendo"
       alarma_id = 123
       tabla_con_estado_mapeo.append(Tabla_programa_mapeo(programa, mapeo, estado, alarma_id))

   return tabla_con_estado_mapeo

#f = open("mapeo.txt", "r")
#programas_mapeo = crear_programa_mapeo(f)
#for entrada in programas_mapeo:
#    print(entrada.programa, entrada.stream, entrada.estado, sep = ' ')


