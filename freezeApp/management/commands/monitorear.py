from django.core.management.base import BaseCommand, CommandError
from freezeApp.models import Multicast
from .tacongelao import monitorear_multicast

class Command(BaseCommand):
    help = 'Imprime multicasts'

    def add_arguments(self, parser):
        parser.add_argument('multicast_id', type = str)
        parser.add_argument('multicast_port', type = str)

    def handle(self, *args, **options):
        multicast_id = options['multicast_id']
        multicast_port = options['multicast_port']

        #Llamo a la funcion monitorear multicast del archivo
        monitorear_multicast(multicast_id, multicast_port)   
      
