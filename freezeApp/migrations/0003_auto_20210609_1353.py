# Generated by Django 2.0.7 on 2021-06-09 13:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('freezeApp', '0002_auto_20210608_1716'),
    ]

    operations = [
        migrations.RenameField(
            model_name='multicast',
            old_name='pid',
            new_name='PID',
        ),
        migrations.AddField(
            model_name='multicast',
            name='puerto_retransmision',
            field=models.CharField(blank=True, max_length=200, unique=True),
        ),
    ]
