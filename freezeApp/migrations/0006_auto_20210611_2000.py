# Generated by Django 2.0.7 on 2021-06-11 20:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('freezeApp', '0005_alarma_reestablecimiento'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alarma',
            name='reestablecimiento',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
    ]
